import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello66Component } from './hello66.component';

describe('Hello66Component', () => {
  let component: Hello66Component;
  let fixture: ComponentFixture<Hello66Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello66Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello66Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
