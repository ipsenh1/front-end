import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello38Component } from './hello38.component';

describe('Hello38Component', () => {
  let component: Hello38Component;
  let fixture: ComponentFixture<Hello38Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello38Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello38Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
