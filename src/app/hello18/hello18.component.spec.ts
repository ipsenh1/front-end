import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello18Component } from './hello18.component';

describe('Hello18Component', () => {
  let component: Hello18Component;
  let fixture: ComponentFixture<Hello18Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello18Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello18Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
