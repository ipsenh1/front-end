import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello81Component } from './hello81.component';

describe('Hello81Component', () => {
  let component: Hello81Component;
  let fixture: ComponentFixture<Hello81Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello81Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello81Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
