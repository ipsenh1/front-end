import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello7Component } from './hello7.component';

describe('Hello7Component', () => {
  let component: Hello7Component;
  let fixture: ComponentFixture<Hello7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
