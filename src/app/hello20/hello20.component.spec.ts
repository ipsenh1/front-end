import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello20Component } from './hello20.component';

describe('Hello20Component', () => {
  let component: Hello20Component;
  let fixture: ComponentFixture<Hello20Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello20Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello20Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
