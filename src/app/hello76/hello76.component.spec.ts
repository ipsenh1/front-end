import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello76Component } from './hello76.component';

describe('Hello76Component', () => {
  let component: Hello76Component;
  let fixture: ComponentFixture<Hello76Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello76Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello76Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
