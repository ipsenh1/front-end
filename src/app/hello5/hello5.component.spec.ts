import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello5Component } from './hello5.component';

describe('Hello5Component', () => {
  let component: Hello5Component;
  let fixture: ComponentFixture<Hello5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
