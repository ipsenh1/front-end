import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello28Component } from './hello28.component';

describe('Hello28Component', () => {
  let component: Hello28Component;
  let fixture: ComponentFixture<Hello28Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello28Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello28Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
