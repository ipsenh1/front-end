import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello95Component } from './hello95.component';

describe('Hello95Component', () => {
  let component: Hello95Component;
  let fixture: ComponentFixture<Hello95Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello95Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello95Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
