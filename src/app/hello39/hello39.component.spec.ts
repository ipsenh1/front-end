import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello39Component } from './hello39.component';

describe('Hello39Component', () => {
  let component: Hello39Component;
  let fixture: ComponentFixture<Hello39Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello39Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello39Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
