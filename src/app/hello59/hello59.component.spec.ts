import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello59Component } from './hello59.component';

describe('Hello59Component', () => {
  let component: Hello59Component;
  let fixture: ComponentFixture<Hello59Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello59Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello59Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
