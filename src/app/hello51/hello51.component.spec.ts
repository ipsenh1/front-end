import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello51Component } from './hello51.component';

describe('Hello51Component', () => {
  let component: Hello51Component;
  let fixture: ComponentFixture<Hello51Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello51Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello51Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
