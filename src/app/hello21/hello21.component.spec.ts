import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello21Component } from './hello21.component';

describe('Hello21Component', () => {
  let component: Hello21Component;
  let fixture: ComponentFixture<Hello21Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello21Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello21Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
