import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello58Component } from './hello58.component';

describe('Hello58Component', () => {
  let component: Hello58Component;
  let fixture: ComponentFixture<Hello58Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello58Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello58Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
