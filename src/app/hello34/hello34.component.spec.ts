import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello34Component } from './hello34.component';

describe('Hello34Component', () => {
  let component: Hello34Component;
  let fixture: ComponentFixture<Hello34Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello34Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello34Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
