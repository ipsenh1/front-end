import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello19Component } from './hello19.component';

describe('Hello19Component', () => {
  let component: Hello19Component;
  let fixture: ComponentFixture<Hello19Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello19Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello19Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
