import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello12Component } from './hello12.component';

describe('Hello12Component', () => {
  let component: Hello12Component;
  let fixture: ComponentFixture<Hello12Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello12Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
