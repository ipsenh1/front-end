import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello45Component } from './hello45.component';

describe('Hello45Component', () => {
  let component: Hello45Component;
  let fixture: ComponentFixture<Hello45Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello45Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello45Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
