import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello55Component } from './hello55.component';

describe('Hello55Component', () => {
  let component: Hello55Component;
  let fixture: ComponentFixture<Hello55Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello55Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello55Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
