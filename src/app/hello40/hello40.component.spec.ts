import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello40Component } from './hello40.component';

describe('Hello40Component', () => {
  let component: Hello40Component;
  let fixture: ComponentFixture<Hello40Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello40Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello40Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
