import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello73Component } from './hello73.component';

describe('Hello73Component', () => {
  let component: Hello73Component;
  let fixture: ComponentFixture<Hello73Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello73Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello73Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
