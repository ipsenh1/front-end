import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello14Component } from './hello14.component';

describe('Hello14Component', () => {
  let component: Hello14Component;
  let fixture: ComponentFixture<Hello14Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello14Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello14Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
