import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello450Component } from './hello450.component';

describe('Hello450Component', () => {
  let component: Hello450Component;
  let fixture: ComponentFixture<Hello450Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello450Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello450Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
