import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hello } from './hello';
import {environment} from '../../environments/environment';

@Injectable()
export class HelloService {
  apiUrl = environment.apiUrl + '/api/hello-world'; // URL to web api

  constructor(private http: HttpClient) {}

  getHello(): Observable<Hello> {
    // console.log('request sent');
    return this.http.get<Hello>(this.apiUrl);
  }
}
