import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello49Component } from './hello49.component';

describe('Hello49Component', () => {
  let component: Hello49Component;
  let fixture: ComponentFixture<Hello49Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello49Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello49Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
