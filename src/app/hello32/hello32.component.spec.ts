import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello32Component } from './hello32.component';

describe('Hello32Component', () => {
  let component: Hello32Component;
  let fixture: ComponentFixture<Hello32Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello32Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello32Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
