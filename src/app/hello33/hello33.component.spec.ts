import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello33Component } from './hello33.component';

describe('Hello33Component', () => {
  let component: Hello33Component;
  let fixture: ComponentFixture<Hello33Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello33Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello33Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
