import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello89Component } from './hello89.component';

describe('Hello89Component', () => {
  let component: Hello89Component;
  let fixture: ComponentFixture<Hello89Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello89Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello89Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
