import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello72Component } from './hello72.component';

describe('Hello72Component', () => {
  let component: Hello72Component;
  let fixture: ComponentFixture<Hello72Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello72Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello72Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
