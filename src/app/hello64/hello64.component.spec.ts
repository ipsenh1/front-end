import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello64Component } from './hello64.component';

describe('Hello64Component', () => {
  let component: Hello64Component;
  let fixture: ComponentFixture<Hello64Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello64Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello64Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
