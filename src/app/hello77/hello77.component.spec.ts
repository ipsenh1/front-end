import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello77Component } from './hello77.component';

describe('Hello77Component', () => {
  let component: Hello77Component;
  let fixture: ComponentFixture<Hello77Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello77Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello77Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
