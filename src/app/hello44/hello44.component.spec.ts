import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello44Component } from './hello44.component';

describe('Hello44Component', () => {
  let component: Hello44Component;
  let fixture: ComponentFixture<Hello44Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello44Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello44Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
