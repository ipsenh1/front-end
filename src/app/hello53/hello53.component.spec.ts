import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello53Component } from './hello53.component';

describe('Hello53Component', () => {
  let component: Hello53Component;
  let fixture: ComponentFixture<Hello53Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello53Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello53Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
