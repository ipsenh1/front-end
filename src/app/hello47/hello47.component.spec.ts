import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello47Component } from './hello47.component';

describe('Hello47Component', () => {
  let component: Hello47Component;
  let fixture: ComponentFixture<Hello47Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello47Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello47Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
