import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello93Component } from './hello93.component';

describe('Hello93Component', () => {
  let component: Hello93Component;
  let fixture: ComponentFixture<Hello93Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello93Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello93Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
