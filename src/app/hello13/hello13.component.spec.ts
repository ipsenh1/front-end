import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello13Component } from './hello13.component';

describe('Hello13Component', () => {
  let component: Hello13Component;
  let fixture: ComponentFixture<Hello13Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello13Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello13Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
