import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello15Component } from './hello15.component';

describe('Hello15Component', () => {
  let component: Hello15Component;
  let fixture: ComponentFixture<Hello15Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello15Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello15Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
