import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello17Component } from './hello17.component';

describe('Hello17Component', () => {
  let component: Hello17Component;
  let fixture: ComponentFixture<Hello17Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello17Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello17Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
