import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello27Component } from './hello27.component';

describe('Hello27Component', () => {
  let component: Hello27Component;
  let fixture: ComponentFixture<Hello27Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello27Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello27Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
