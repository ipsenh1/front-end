import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello31Component } from './hello31.component';

describe('Hello31Component', () => {
  let component: Hello31Component;
  let fixture: ComponentFixture<Hello31Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello31Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello31Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
