import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello60Component } from './hello60.component';

describe('Hello60Component', () => {
  let component: Hello60Component;
  let fixture: ComponentFixture<Hello60Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello60Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello60Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
