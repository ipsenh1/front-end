import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello35Component } from './hello35.component';

describe('Hello35Component', () => {
  let component: Hello35Component;
  let fixture: ComponentFixture<Hello35Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello35Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello35Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
