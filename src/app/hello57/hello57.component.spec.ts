import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello57Component } from './hello57.component';

describe('Hello57Component', () => {
  let component: Hello57Component;
  let fixture: ComponentFixture<Hello57Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello57Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello57Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
