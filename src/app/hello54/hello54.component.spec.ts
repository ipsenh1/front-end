import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello54Component } from './hello54.component';

describe('Hello54Component', () => {
  let component: Hello54Component;
  let fixture: ComponentFixture<Hello54Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello54Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello54Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
