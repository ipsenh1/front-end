import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello61Component } from './hello61.component';

describe('Hello61Component', () => {
  let component: Hello61Component;
  let fixture: ComponentFixture<Hello61Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello61Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello61Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
