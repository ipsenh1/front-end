import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello8Component } from './hello8.component';

describe('Hello8Component', () => {
  let component: Hello8Component;
  let fixture: ComponentFixture<Hello8Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello8Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
