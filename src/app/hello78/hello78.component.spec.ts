import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello78Component } from './hello78.component';

describe('Hello78Component', () => {
  let component: Hello78Component;
  let fixture: ComponentFixture<Hello78Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello78Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello78Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
