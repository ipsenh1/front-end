import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello36Component } from './hello36.component';

describe('Hello36Component', () => {
  let component: Hello36Component;
  let fixture: ComponentFixture<Hello36Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello36Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello36Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
